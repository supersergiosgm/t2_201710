package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements ILista {

	private int top;

	private ListaDobleEncadenada<T> lista;

	public Stack() {
		top = -1;
		lista = new ListaDobleEncadenada<T>();
	}

	public void push(T pushValue) {
		lista.agregarElementoFinal(pushValue);
		top ++;
	}

	public T pop() throws Exception{

		if (top == -1) {
			throw new Exception("La pila se encunetra vac�a");
		}

		T item = null;

		item = lista.darElemento(top);
		lista.eliminar(top);

		top --;

		return item;
	}

	public int size()
	{
		return lista.darNumeroElementos();
	}

	public boolean isEmpty()
	{
		return lista.isEmpty();
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(Object elem) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object darElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		return false;
	}


}
