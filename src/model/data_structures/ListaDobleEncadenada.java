package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> first;
	
	private NodoDoble<T> actual;

	private NodoDoble<T> last;

	private int size;

	public ListaDobleEncadenada()
	{
		size = 0;
	}

	public boolean isEmpty(){return size == 0; }

	@Override
	public Iterator<T> iterator() {
		return new DoublyLinkedListIterator<T>();

	}


	private class DoublyLinkedListIterator<T> implements Iterator<T> {
		private NodoDoble<T> ante;

		public DoublyLinkedListIterator()
		{
			ante = new NodoDoble<T>(null);
			ante.setNext((NodoDoble<T>) first);
		}

		public boolean hasNext()      { if (ante.getNext() !=null) {return true;}else{return false;}}

		public T next() {
			if (!hasNext()){ throw new NoSuchElementException( "No hay elemento siguiente");}
			ante = ante.getNext();
			T item = ante.giveItem();
			return item;
		}

		@Override
		public void remove() {
		}


	}

	@Override
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> newnode = new NodoDoble<T>(elem);
		if(first==null){
			first=newnode;
			actual=newnode;
			last=newnode;
			newnode.setNext(null);
			newnode.setPrev(null);
		}else{
			newnode.setPrev(last);
			last.setNext(newnode);
			last=newnode;
		}
		size++;
	}

	@Override
	public T darElemento(int pos) {
		Iterator<T> it = iterator();
		int i=0;
		T rta=null;
		while(it.hasNext() && i<=pos)
		{
			rta=it.next();
			if(i==pos)
			{
				return rta;
			}
			i++;
		}
		return rta;
	}


	@Override
	public int darNumeroElementos() {
		return size;
	}

	@Override
	public T darElementoPosicionActual() throws NullPointerException{
		return actual.giveItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(actual==null)return false;
		else if(actual.getNext()!=null)
		{
			actual=actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if (actual == null) {return false;}
		else if (actual.getPrev() != null){actual = actual.getPrev(); return true;}
		else {return false;}
	}
	
	public NodoDoble<T> getFirst()
	{
		return first;
	}
	
	public NodoDoble<T> getActual()
	{
		return actual;
	}
	
	public void resetActual()
	{
		actual=first;
	}
	
	public void increase()
	{
		size++;
	}

	public void setActual(NodoDoble<T> x) {

		actual=x;
	}
	
	public void setFirst(NodoDoble<T> x)
	{
		first=x;
	}
	
	public void eliminar(int pos){
		if (first == null) {
			return;
		}
		
		if (pos == 0) {
			first = first.getNext();
			if (first == null) {
				last = null;
			}
		}
		
		else{
			NodoDoble<T> temp = first;
		       for (int index = 0; index < pos; index++){
		            temp = temp.getNext();
		        }
		        temp.getNext().setPrev(temp.getPrev());
		        temp.getPrev().setNext(temp.getNext());
		    }
		    size--;
		}
	}
	
