package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements ILista<T> {	

	private ListaDobleEncadenada<T> lista;

	public Queue(){

		lista = new ListaDobleEncadenada<T>();
	}


	public void enqueue(T elem) {
		lista.agregarElementoFinal(elem);
	}

	public T dequeue() {
		T item = null;
		if (this.size()==0) {
			return item;
		}else{
			item = lista.darElemento(0);
			lista.eliminar(0);
			return item;
		}
	}

	public int size(){
		int size = lista.darNumeroElementos();
		return size;
	}

	public T peek() {
		return lista.darElementoPosicionActual();
	}

	public boolean isEmpty(){
		if (this.size() == 00) {
			return true;
		}
		else{
			return false;
		}
	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		return false;
	}
}