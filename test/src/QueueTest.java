

import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase{
	
	private Queue<String> principal;
	
	private final static String T1="Hola";
	private final static String T2="soy";
	private final static String T3="JUAN";
	private final static String T4="GARCIA";
	private final static String T5="de";
	private final static String T6="Estructuras";

	private final static String RTA1="A";
	
	public void setupEscenario1()
	{
		principal=new Queue<String>();
	}

	public void setupEscenario2()
	{
		principal=new Queue<String>();
		principal.enqueue("Hola");
	}

	public void setupEscenario3()
	{
		principal=new Queue<String>();
		principal.enqueue(T1);
		principal.enqueue(T2);		
		principal.enqueue(T3);
		principal.enqueue(T4);
		principal.enqueue(T5);
		principal.enqueue(T6);
	}
	
	public void testEnqueue()
	{
		setupEscenario1();
		principal.enqueue(RTA1);
		String rta=principal.dequeue();
		assertEquals("No se agregó el elemento esperado",RTA1,rta);
	}
	
	public void testEnqueue2()
	{
		setupEscenario3();
		principal.enqueue(RTA1);
		String rta=principal.dequeue();
		assertEquals("No se agregó el elemento esperado",T1,rta);
		rta=principal.dequeue();
		rta=principal.dequeue();
		rta=principal.dequeue();
		assertEquals("No se agregó el elemento esperado",T4,rta);
		rta=principal.dequeue();
		rta=principal.dequeue();
		assertEquals("No se agregó el elemento esperado",T6,rta);
		rta=principal.dequeue();
		assertEquals("No se agregó el elemento esperado",RTA1,rta);
	}
	
	public void testDequeue()
	{
		setupEscenario3();
			assertEquals("No es el elemento esperado",T1,principal.dequeue());

	}


	public void testDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("No es el tamaño esperado",0,principal.size());

		setupEscenario3();
		assertEquals("No es el tamaño esperado",6,principal.size());

	}
	
	/*public void testDarElementoPosicionActual()
	{
		setupEscenario1();
		try
		{
			principal.darElementoPosicionActual();
			fail();
		}
		catch(NoSuchElementException e)
		{

		}
		setupEscenario3();
		assertEquals("No es el elemento que se tiene actualmente",T1,principal.darElementoPosicionActual());
		principal.avanzarSiguientePosicion();
		principal.avanzarSiguientePosicion();
		assertEquals("No es el elemento que se tiene actualmente",T3,principal.darElementoPosicionActual());
	}*/
}
