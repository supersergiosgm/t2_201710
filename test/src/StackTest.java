import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{

	private Stack<String> principal;

	private final static String T1="Hola";
	private final static String T2="soy";
	private final static String T3="JUAN";
	private final static String T4="GARCIA";
	private final static String T5="de";
	private final static String T6="Estructuras";

	private final static String RTA1="A";

	public void setupEscenario1()
	{
		principal=new Stack<String>();
	}

	public void setupEscenario2()
	{
		principal=new Stack<String>();
		principal.push("Hola");
	}

	public void setupEscenario3()
	{
		principal=new Stack<String>();
		principal.push(T1);
		principal.push(T2);		
		principal.push(T3);
		principal.push(T4);
		principal.push(T5);
		principal.push(T6);
	}

	public void testPush()
	{
		setupEscenario1();
		principal.push(RTA1);
		String rta = "B";
		try {
			rta = (String) principal.pop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("No se agregó el elemento esperado",RTA1,rta);
	}

	public void testPop()
	{
		setupEscenario3();
		String [] esperados= new String [6];
		esperados[0]=T1;
		esperados[1]=T2;
		esperados[2]=T3;
		esperados[3]=T4;
		esperados[4]=T5;
		esperados[5]=T6;
		try {
			assertEquals("No es el elemento esperado",esperados[5],principal.pop());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void testSize()
	{
		setupEscenario1();
		assertEquals("No es el tamaño esperado",0,principal.size());

		setupEscenario3();
		assertEquals("No es el tamaño esperado",6,principal.size());

	}

	public void testIsEmpty()
	{
		setupEscenario1();
		assertEquals("Deber�a encontrarse vac�a",true,principal.isEmpty());
		
		setupEscenario3();
		assertEquals("La pila deber�a contener elementos", false,principal.isEmpty());
	}
}
