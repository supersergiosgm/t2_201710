import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase{

	private ListaDobleEncadenada<String> principal;

	private final static String T1="Yo";
	private final static String T2="soy";
	private final static String T3="Juan";
	private final static String T4="Garcia";
	private final static String T5="de";
	private final static String T6="Estructuras";

	private final static String RTA2="A";
	private final static String RTA3="B";


	public void setupEscenario1()
	{
		principal=new ListaDobleEncadenada<String>();
	}

	public void setupEscenario2()
	{
		principal=new ListaDobleEncadenada<String>();
		principal.agregarElementoFinal("Hola");
	}

	public void setupEscenario3()
	{
		principal=new ListaDobleEncadenada<String>();
		principal.agregarElementoFinal(T1);
		principal.agregarElementoFinal(T2);		
		principal.agregarElementoFinal(T3);
		principal.agregarElementoFinal(T4);
		principal.agregarElementoFinal(T5);
		principal.agregarElementoFinal(T6);
	}

	public void testAgregarElementoFinal1()
	{
		setupEscenario1();
		principal.agregarElementoFinal(RTA2);
		String rta = principal.darElementoPosicionActual();
		assertEquals("No se agregó el elemento esperado",RTA2,rta);
	}

	public void testAgregarElementoFinal2()
	{
		setupEscenario3();
		principal.agregarElementoFinal(RTA2);
		principal.agregarElementoFinal(RTA3);
		String rta = principal.darElemento(0);
		assertEquals("No se agregó el elemento esperado",T1,rta);
		rta=principal.darElemento(3);
		assertEquals("No se agregó el elemento esperado",T4,rta);
		rta=principal.darElemento(5);
		assertEquals("No se agregó el elemento esperado",T6,rta);
		rta=principal.darElemento(6);
		assertEquals("No se agregó el elemento esperado",RTA2,rta);
	}

	public void testDarElemento()
	{
		setupEscenario3();
		String [] esperados= new String [6];
		esperados[0]=T1;
		esperados[1]=T2;
		esperados[2]=T3;
		esperados[3]=T4;
		esperados[4]=T5;
		esperados[5]=T6;
		for(int i=0;i<esperados.length;i++)
		{

			assertEquals("No es el elemento esperado",esperados[i],principal.darElemento(i));
		}

	}

	public void testDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("No es el tamaño esperado",0,principal.darNumeroElementos());

		setupEscenario3();
		assertEquals("No es el tamaño esperado",6,principal.darNumeroElementos());

	}

	public void testDarElementoPosicionActual()
	{
		setupEscenario1();
		try
		{
			principal.darElementoPosicionActual();
			fail();
		}
		catch(NullPointerException e)
		{

		}
		setupEscenario3();
		assertEquals("No es el elemento que se tiene actualmente",T1,principal.darElementoPosicionActual());
		principal.avanzarSiguientePosicion();
		principal.avanzarSiguientePosicion();
		assertEquals("No es el elemento que se tiene actualmente",T3,principal.darElementoPosicionActual());
	}

	public void testAvanzarSiguientePosicion()
	{
		setupEscenario1();
		assertFalse("No debería avanzar",principal.avanzarSiguientePosicion());
		setupEscenario3();
		String [] esperados= new String [6];
		esperados[0]=T1;
		esperados[1]=T2;
		esperados[2]=T3;
		esperados[3]=T4;
		esperados[4]=T5;
		esperados[5]=T6;
		int i=1;
		while(principal.avanzarSiguientePosicion())
		{
			assertEquals("No avanzó correctamente",esperados[i],principal.darElementoPosicionActual());
			i++;
		}
		assertFalse("No debería avanzar",principal.avanzarSiguientePosicion());

	}

	public void testRetrocederPosicionAnterior()
	{
		setupEscenario1();
		assertFalse("No debería retroceder",principal.retrocederPosicionAnterior());

		setupEscenario3();
		String [] esperados= new String [6];
		esperados[0]=T1;
		esperados[1]=T2;
		esperados[2]=T3;
		esperados[3]=T4;
		esperados[4]=T5;
		esperados[5]=T6;
		while(principal.avanzarSiguientePosicion())
		{

		}
		int i=4;
		while(principal.retrocederPosicionAnterior())
		{
			assertEquals("No retrocedió correctamente",esperados[i],principal.darElementoPosicionActual());
			i--;
		}
		assertFalse("No debería retroceder",principal.retrocederPosicionAnterior());

	}

	public void testEliminar(){

		setupEscenario2();
		principal.eliminar(0);
		assertEquals("La lista deber�a estar vac�a",null,principal.darElemento(0));

		setupEscenario3();
		principal.eliminar(4);
		assertEquals("No est� eliminando correctamente",T6, principal.darElemento(4));
	}

}
